const express = require('express');
const router = express.Router();

function stackRouter(wss) {

  var counter = 0;
  var stacks = {};

  function send(stack) {
    wss.clients.forEach((ws) => {
      ws.send(JSON.stringify(stack));
    });
  }

  router.get('/new', (req, res) => {
    var stack = {
      type: "stack",
      id: `stack-${counter}`,
      data: []
    };
    stacks[stack.id] = stack;
    send(stack);
    res.send(200, `stack-${counter}`);
    counter++;
  });

  router.get('/:id/push/:item', (req, res) => {
    var stack = stacks[req.params.id];
    stack.data.push(req.params.item);
    send(stack);
    res.send(200);
  });

  router.get('/:id/pop', (req, res) => {
    var stack = stacks[req.params.id];
    const val = stack.data.pop();
    send(stack);
    res.send(200, val);
  });

  return router;
}

module.exports = stackRouter;
