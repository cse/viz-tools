const express = require('express');
const router = express.Router();

function queueRouter(wss) {

  var counter = 0;
  var queues = {};

  function send(queue) {
    wss.clients.forEach((ws) => {
      ws.send(JSON.stringify(queue));
    });
  }

  router.get('/new', (req, res) => {
    var queue = {
      type: "queue",
      id: `queue-${counter}`,
      data: []
    };
    queues[queue.id] = queue;
    send(queue);
    res.send(200, `queue-${counter}`);
    counter++;
  });

  router.get('/:id/enqueue/:item', (req, res) => {
    var queue = queues[req.params.id];
    queue.data.unshift(req.params.item);
    send(queue);
    res.send(200);
  });

  router.get('/:id/dequeue', (req, res) => {
    var queue = queues[req.params.id];
    const val = queue.data.pop();
    send(queue);
    res.send(200, val);
  });

  return router;
}

module.exports = queueRouter;
