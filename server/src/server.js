const http = require('http');
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const ws = require('ws');
const stackRouter = require('./api/stack-router');
const queueRouter = require('./api/queue-router');

const PORT = 8080;

const app = express();
const config = require('../webpack.config.js');
const compiler = webpack(config);
const server = http.createServer(app);
const wss = new ws.Server({ server });

// Tell express to use the api routes
app.use('/stacks', stackRouter(wss));
app.use('/queues', queueRouter(wss));

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath,
}));

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    // TODO: Handle client messages
    // console.log('received: %s', message);
  });
});

server.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`)
    console.log('Press Ctrl+C to quit.')
});
