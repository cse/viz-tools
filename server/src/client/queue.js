import * as d3 from 'd3';

/** @function queue
  * A function that renders the visualizaiton of a queue.
  * @param {string} id - a unique identifier for the queue
  * @param {Array} data - the contents of the queue
  */
export default function queue(id, data) {
  console.log(id, data);
  var viz = d3.select('#visualizations')
    .select('#' + id);

  // Append a new queue visualization if one does not exist
  if(viz.empty()) {
    viz = d3.select('#visualizations')
      .append('div')
      .attr('id', id)
      .attr('class', 'queue');
    viz.append('h4')
      .text('Queue');
    viz.append('div')
      .attr('class', 'contents');
  }

  viz = viz.select('.contents').selectAll("div")
    .data(data)
    .text(function(d) { console.log(d); return d; });

  // Enter…
  viz.enter().append("div")
      .text(function(d) { return d; });

  // Exit…
  viz.exit().remove();
}
