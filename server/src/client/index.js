import * as d3 from 'd3';
import './viz.css';
import stack from './stack';
import queue from './queue';

// Create the page layout
const body = d3.select('body');

body.append('h1')
  .text("Data Structure Visualizations");

body.append('div')
  .attr('id', 'visualizations');

// Create the websocket
const protocol = (window.location.protocol === "https") ? "wss" : "ws";
const socket = new WebSocket(`${protocol}://${window.location.host}`);

// Connection opened
socket.addEventListener('open', function (event) {
  // TODO: Send connection message?
  //socket.send('Hello Server!');
});

// Listen for messages from the socket
socket.addEventListener('message', function(event) {
  const viz = JSON.parse(event.data);
  switch(viz.type) {
    case 'stack':
      stack(viz.id, viz.data);
      break;
    case 'queue':
      queue(viz.id, viz.data);
      break;
  }
});
