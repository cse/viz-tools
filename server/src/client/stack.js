import * as d3 from 'd3';

/** @function stack
  * A function that renders the visualizaiton of a stack.
  * @param {string} id - a unique identifier for the stack
  * @param {Array} data - the contents of the stack
  */
export default function stack(id, data) {
  var stack = d3.select('#visualizations')
    .select('#' + id);

  // Append a new stack visualization if one does not exist
  if(stack.empty()) {
    stack = d3.select('#visualizations')
      .append('div')
      .attr('id', id)
      .attr('class', 'stack');
    stack.append('h4')
      .text('Stack');
    stack.append('div')
      .attr('class', 'contents');
  }

  stack = stack.select('.contents').selectAll("div")
    .data(data)
    .text(function(d) { console.log(d); return d; });

  // Enter…
  stack.enter().append("div")
      .text(function(d) { return d; });

  // Exit…
  stack.exit().remove();
}
