import requests

PORT = 8080

class Stack:

    def __init__(self):
        url = "http://localhost:{}/stacks/new".format(PORT)
        response = requests.get(url)
        if response.status_code == 200:
            self.id = response.text
        else:
            raise RuntimeError

    def push(self, item):
        url = "http://localhost:{}/stacks/{}/push/{}".format(PORT, self.id, item)
        response = requests.get(url)
        if response.status_code != 200:
             raise RuntimeError

    def pop(self, item):
        url = "http://localhost:{}/stacks/{}/pop".format(PORT, self.id)
        response = requests.get(url)
        if(response.status_code != 200):
            raise RuntimeError

class Queue:

    def __init__(self):
        url = "http://localhost:{}/queues/new".format(PORT)
        response = requests.get(url)
        if response.status_code == 200:
            self.id = response.text
        else:
            raise RuntimeError

    def enqueue(self, item):
        url = "http://localhost:{}/queues/{}/enqueue/{}".format(PORT, self.id, item)
        response = requests.get(url)
        if response.status_code != 200:
             raise RuntimeError

    def dequeue(self, item):
        url = "http://localhost:{}/queues/{}/dequeue".format(PORT, self.id)
        response = requests.get(url)
        if(response.status_code != 200):
            raise RuntimeError
